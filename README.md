# Clickon Pluton SDK #

Create a composer file and run composer install, for example:


```
#!json

{
  "name": "jprueba/clickon-sdk-test",
  "authors": [
    {
      "name": "Juan Prueba",
      "email": "juan@prueba.com"
    }
  ],
  "require": {
    "ebowe/clickon-pluton-php-sdk": "dev-master"
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "https://bitbucket.org/ebowe/clickon-pluton-php-sdk.git"
    }
  ]
}
```

Usage example:


```
#!php

<?php

require_once './vendor/autoload.php';

use ClickonPluton\ClickonPluton;

$hmtl = "<html><body><a href='http://www.lanacion.com.ar'>La Nacion</a><a href='http://www.clarin.com.ar'>Clarin</a></body></html>";


$pluton = new ClickonPluton("http://127.0.0.1:8000/", "Secret key");
echo $pluton->processHtml($hmtl, "cliente", "campana", "juan@prueba.com");
```