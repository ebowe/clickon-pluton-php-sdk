<?php

namespace ClickonPluton;

use JWT;
use QueryPath;

/**
 * Class ClickonPluton
 * @package ClickonPluton
 */
class ClickonPluton
{
    const TRACK_PATH = '/click/';
    const OPEN_PATH = '/open/';
    const LIST_UNSUBSCRIBE_PATH = '/list-unsubscribe/';

    /**
     * @var string $host Tracking host
     */
    private $host;

    /**
     * @var string $privateKey JWT encoding key
     */
    private $privateKey;

    public function __construct($host, $privateKey)
    {
        $this->host = rtrim($host, "/");
        $this->privateKey = $privateKey;
    }

    /**
     * @param $html
     * @param $campaign
     * @param $email
     * @return mixed
     */
    public function processHtml($html, $client, $campaign, $email)
    {
        $qp = qp($html);

        foreach ($qp->find("a") as $a) {
            $url = trim($a->attr('href'));
            $path = $a->hasClass('unsubscribe') ? self::LIST_UNSUBSCRIBE_PATH : self::TRACK_PATH;
            $href = self::encodeUrl($url, $client, $campaign, $email, $path);

            $a->attr('href', $href);
        }

        $qp->find("body")->append($this->generateOpenImage($client, $campaign, $email));

        return $qp->html();
    }

    public function encodeUrl($url, $client, $campaign, $email, $path = self::TRACK_PATH)
    {
        return $this->host . $path . JWT::encode(self::getRawPayload($client, $campaign, $email, $url), $this->privateKey, 'HS256');
    }

    public function encodeListUnsubscribeUrl($url, $client, $campaign, $email)
    {
        return $this->encodeUrl($url, $client, $campaign, $email, self::LIST_UNSUBSCRIBE_PATH);
    }

    public function generateOpenImage($client, $campaign, $email)
    {
        return '<img src="' . $this->host . self::OPEN_PATH . JWT::encode(self::getRawPayload($client, $campaign, $email), $this->privateKey, 'HS256') . '.gif" width="1" height="1" />';
    }

    public function buildHeaders($email, $campaign, $client)
    {
        $date = date('Y-m-d H:i:s');
        $messageId = base64_encode("{$email}_{$campaign}_{$date}");

        return [
            'X-SF-ID' => $messageId,
            'X-SF-CAMPAIGN' => $campaign,
            'X-SF-CLIENT' => $client,
        ];
    }

    private static function getRawPayload($client, $campaign, $email, $url = null)
    {

        $json = ["client" => $client, "campaign" => $campaign, "contact" => $email];

        if (!empty($url)) {
            $json['url'] = $url;
        }

        return $json;
    }
}